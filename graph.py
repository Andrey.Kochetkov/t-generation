from typing import List


graph = {
    'A': {'B', 'C'},
    'B': {'A', 'D', 'E'},
    'C': {'A', 'F'},
    'D': {'B'},
    'E': {'B', 'F'},
    'F': {'C', 'E'}
}


def bfs(graph, start: str):
    visited, queue = set(), [start]
    while queue:
        vertex = queue.pop(0)
        if vertex not in visited:
            visited.add(vertex)
            print(vertex)
            neightbour_vertexes = graph[vertex]
            queue += list(neightbour_vertexes - visited)
    return visited


def bfs_paths(graph: dict, start: str, goal: str):
    init_vertex: str = start
    init_path: List[str] = [start]
    queue = [(init_vertex, init_path)]
    while queue:
        vertex, path = queue.pop(0)
        not_visited_neightbour_vertexes = graph[vertex] - set(path)  # множество не посещенных соседей
        for current_vertex in not_visited_neightbour_vertexes:
            if current_vertex == goal:
                yield path + [current_vertex]
            else:
                queue.append((current_vertex, path + [current_vertex]))


print(list(bfs_paths(graph, 'A', 'F')))  # [['A', 'C', 'F'], ['A', 'B', 'E', 'F']]
