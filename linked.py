# пример реализации связного списка в питоне

class Node:
    def __init__(self, value: int, next: 'Node'=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.head = None

    def insert_before_head(self, newdata):
        new = Node(newdata)

        # Update the new nodes next val to existing node
        new.head = self.head
        self.head = new


linked = LinkedList()
linked.head = Node("Mon")
e2 = Node("Tue")
e3 = Node("Wed")

linked.head.next = e2
e2.next = e3

linked.insert_before_head("Sun")


def reverse(cursor):
    reversed_list = None
    while cursor:
        temp = reversed_list
        next_item = cursor.next
        reversed_list = cursor
        reversed_list.next = temp
        cursor = next_item

    return reversed_list


head = Node(1, Node(2, Node(3)))

head = reverse(head)
