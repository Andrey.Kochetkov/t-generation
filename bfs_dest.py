# BFS на ориентированном графе, дейсктра


from collections import OrderedDict


graph = OrderedDict({
    0: {1, 2},
    1: {3, 5, 4},
    2: {5},
    3: {5},
    4: {5},
    5: {}
})


def bfs(input_graph: OrderedDict, start_vertex: int):
    n = len(graph)
    distances = [n] * n
    distances[start_vertex] = 0
    queue = [start_vertex]
    while queue:
        current_vertex = queue.pop(0)
        for i in input_graph[current_vertex]:
            if distances[i] > distances[current_vertex] + 1:
                distances[i] = distances[current_vertex] + 1
                print(i)
                queue.append(i)

    return distances


# print(bfs(graph, 0))


def bfs_shortest_path(input_graph, start_vertex, goal_vertex):
    n = len(graph)
    distances = [n] * n
    distances[start_vertex] = 0
    queue = [start_vertex]
    parents = [-1] * n
    while queue:
        current_vertex = queue.pop(0)
        for i in input_graph[current_vertex]:
            if distances[i] > distances[current_vertex] + 1:
                parents[i] = current_vertex
                distances[i] = distances[current_vertex] + 1
                queue.append(i)
    if distances[goal_vertex] == n:
        return []

    paths = []
    vertex = goal_vertex
    while vertex != -1:
        paths.insert(0, vertex)
        vertex = parents[vertex]

    return paths


# print(bfs_shortest_path(graph, 0, 5))

from math import inf
from typing import NamedTuple


class Pair(NamedTuple):
    destination_vertex: int
    weight: int


def dijkstra(input_graph: OrderedDict, start_vertex: int):
    n = len(graph)
    distances = [inf] * n
    distances[start_vertex] = 0
    visited = []
    while True:
        available_vertexes = list(set(input_graph.keys()) - set(visited))
        if not available_vertexes:
            break

        sorted_vertexes = sorted([(i, distances[i]) for i in available_vertexes], key=lambda x: x[1])
        current_vertex = sorted_vertexes[0][0]

        for pair in input_graph[current_vertex]:
            destination_vertex = pair.destination_vertex
            weight = pair.weight
            if distances[destination_vertex] > distances[current_vertex] + weight:
                distances[destination_vertex] = distances[current_vertex] + weight

        visited.append(current_vertex)

    return distances


graph = OrderedDict({
    0: {Pair(1, 10), Pair(2, 30), Pair(3, 50), Pair(4, 10)},
    1: {},
    2: {},
    3: {Pair(1, 40), Pair(2, 10)},
    4: {Pair(2, 10), Pair(3, 30)}
})
print(dijkstra(graph, 0))
