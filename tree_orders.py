#  BFS и DFS на деревьях

import datetime
from typing import List


class TreeNode(object):
    def __init__(self, index, value):
        self.index = index
        self.children: List['TreeNode'] = []
        self.value = value

    def __repr__(self):
        return str(self.index)

    def add_child(self, node):
        self.children.append(node)

    def get_children(self):
        return self.children

    def get_rev_children(self):
        children = self.children[:]
        children.reverse()
        return children


def DFS(root):
    nodes = []
    stack = [root]

    while stack:
        current_node = stack[0]
        print("Visiting node", str(current_node), " with value", current_node.value)
        stack = stack[1:]  # выкидываем первый элемент
        nodes.append(current_node)

        stack = current_node.get_children() + stack

    return nodes


def BFS(root):
    nodes = []
    stack = [root]

    while stack:
        current_node = stack[0]
        print("Visiting node", str(current_node), " with value", current_node.value)
        stack = stack[1:]
        nodes.append(current_node)
        stack = stack + current_node.get_children()

    return nodes


def get_example_tree():
    # create nodes
    root = TreeNode("a0", 12323)

    b0 = TreeNode("b0", 13224)
    b1 = TreeNode("b1", 3456)
    b2 = TreeNode("b2", 2134)

    c0 = TreeNode("c0", 42345)
    c1 = TreeNode("c1", 522)
    c2 = TreeNode("c2", 624123)

    d0 = TreeNode("d0", 6243)
    d1 = TreeNode("d1", 62143)

    e0 = TreeNode("e0", 6143)

    # add nodes
    root.add_child(b0)
    root.add_child(b1)
    root.add_child(b2)

    b0.add_child(c0)
    b0.add_child(c1)

    b1.add_child(c2)

    c0.add_child(d0)

    c2.add_child(d1)

    d1.add_child(e0)

    return root


if __name__ == "__main__":

    root = get_example_tree()  # the tree

    print("\n------------------------- DFS -------------------------\n")
    start = datetime.datetime.now()
    DFS(root)
    done = datetime.datetime.now()
    elapsed = done - start
    print("\nFinished in ", elapsed.microseconds, " microseconds")

    print("\n------------------------- BFS -------------------------\n")
    start = datetime.datetime.now()
    BFS(root)
    done = datetime.datetime.now()
    elapsed = done - start
    print("\nFinished in ", elapsed.microseconds, " microseconds")
