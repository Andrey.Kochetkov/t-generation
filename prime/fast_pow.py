# возведение числа в степень


def expt(b, n):
    if n == 0:
        return 1
    return b * expt(b, n - 1)


def expt_iter(b, n):
    def exptIter(counter, product):
        if counter == 0:
            return product
        return exptIter(counter - 1, b * product)

    return exptIter(n, 1)


def even(n):
    if n % 2 == 0:
        return 1
    return 0


def fast_exp(b, n):
    if n == 0:
        return 1
    if even(n):
        return fast_exp(b, n / 2) ** 2
    return b * fast_exp(b, n - 1)


def fast_pow(a, k):  # возведем а в степень k
    accum = 1
    while k:
        if k % 2:
            accum *= a
        a *= a
        k //= 2
    return accum
