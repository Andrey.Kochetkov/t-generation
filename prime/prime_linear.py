# проверка на простоту за корень


def is_prime(n: int) -> bool:
    if n == 1:
        return False
    for i in range(2, n):  # начинаем с 2, так как на 1 все делится; n не включается
        if n % i == 0:
            return False
    return True


for i in range(1, 10):
    print(i, is_prime(i))
