# проверка на простоту за квадрат


def is_prime(n: int) -> bool:
    if n == 1:
        return False
    i = 2
    while i * i <= n:
        if n % i == 0:
            return False
        i += 1
    return True


for i in [1, 2, 3, 10, 11, 12, 10 ** 9 + 6, 10 ** 9 + 7]:
    print(i, is_prime(i))
