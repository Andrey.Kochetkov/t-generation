# разложение числа на простые множители

def factorize(number: int) -> list:
    factors = []
    i = 2
    while i * i <= number:  # перебираем простой делитель
        while number % i == 0:  # пока N на него делится
            number //= i  # делим N на этот делитель
            factors.append(i)
        i += 1
    # возможно, в конце N стало большим простым числом,
    # у которого мы дошли до корня и поняли, что оно простое
    # его тоже нужно добавить в разложение
    if number > 1:
        factors.append(number)
    return factors


for i in [10 ** 9]:
    print(i, '=', ' x '.join(str(x) for x in factorize(i)))
