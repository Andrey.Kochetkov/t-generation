# решето Эратосфена

N = 50

is_prime = [True] * (N + 1)  # просто заполняем массив
is_prime[0], is_prime[1] = False, False
for i in range(2, N + 1):  # можно и до sqrt(N)
    if is_prime[i] is True:
        for j in range(2 * i, N + 1, i):  # идем с шагом i, можно начиная с i * i
            is_prime[j] = False

for i in range(1, N + 1):
    if is_prime[i]:
        print(i)

for i in range(1, N + 1):
    print(i, '\t', is_prime[i])

N = 30
primes = []
min_d = [0] * (N + 1)

for i in range(2, N + 1):
    if min_d[i] == 0:
        min_d[i] = i
        primes.append(i)
    for p in primes:
        if p > min_d[i] or i * p > N:
            break
        min_d[i * p] = p
    print(i, min_d)
print(min_d)
print(primes)