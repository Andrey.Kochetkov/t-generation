# задача про переговорную комнату и заявки

from typing import List, Tuple


def get_meetings(array: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    result = []
    sorted_array = sorted(array, key=lambda x: x[1])
    while True:
        if not sorted_array:
            break
        item = sorted_array.pop(0)
        left, right = item
        sorted_array = list(filter(lambda x: x[0] >= right, sorted_array))
        result.append(item)
    return result


assert 2 == len(get_meetings([(1, 5), (2, 3), (3, 4)]))

amount = int(input())
input_array = []
for _ in range(amount):
    a, b = map(int, input().split())
    input_array.append((a, b))
print(get_meetings(input_array))
