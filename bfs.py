# BFS на неориентированном графе

from typing import List


graph = {
    'A': {'B', 'C'},
    'B': {'A', 'D', 'E'},
    'C': {'A', 'F'},
    'D': {'B'},
    'E': {'B', 'F'},
    'F': {'C', 'E'}
}


def bfs(graph, start):
    visited, queue = set(), [start]
    while queue:
        vertex = queue.pop(0)
        if vertex not in visited:
            visited.add(vertex)
            print(vertex)
            queue.extend(graph[vertex] - visited)
    return visited


def bfs_paths(graph: dict, start: str, goal: str):
    init_vertex: str = start
    init_path: List[str] = [start]
    queue = [(init_vertex, init_path)]
    while queue:
        (vertex, path) = queue.pop(0)
        for current_vertex in graph[vertex] - set(path):  # graph[vertex] - set(path) - множество не посещенных соседей
            if current_vertex == goal:
                yield path + [current_vertex]
            else:
                queue.append((current_vertex, path + [current_vertex]))


def shortest_path(graph, start, goal):
    try:
        return next(bfs_paths(graph, start, goal))
    except StopIteration:
        return None


list(shortest_path(graph, 'A', 'F'))  # ['A', 'C', 'F']

print(list(bfs_paths(graph, 'A', 'F')))  # [['A', 'C', 'F'], ['A', 'B', 'E', 'F']]

# print(bfs(graph, 'A'))  # {'B', 'C', 'A', 'F', 'D', 'E'}
