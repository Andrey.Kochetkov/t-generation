# пример реализации бинарного дерева в питоне

from dataclasses import dataclass
from typing import Any


class Tree:
    def __init__(self, data: Any, left: 'Tree' = None, right: 'Tree' = None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


@dataclass
class Tree:
    data: Any
    left: 'Tree' = None
    right: 'Tree' = None


left = Tree(2)
right = Tree(3)

tree = Tree(1, left, right)
